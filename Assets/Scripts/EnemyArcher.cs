﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArcher : MonoBehaviour {

    public float health = 60f;
    public int range = 8;
    public int moneyValue = 55;
    public Transform target;
    public float damage = 1f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    private float time = 4f;
    // Use this for initialization
    int speed = 5;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("Creeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
           // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
             //   Debug.Log("Am gasit un noob");
                return;
            }

        }
        
    }

    private void Atack()
    {
        GetComponent<Move>().inRange = true;
       //target.GetComponent<EnemyArcher>().TakeDamage(damage);
    }
    public void TakeDamage(float damage)
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            
            WillTakeDamage = true;
            damageToTake = damage;
        }
    }
    public void Die()
    {
        //GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 5;
        int damageOnSoldier = 2;
        int damageOnBarbarian = 3;
      //  Debug.Log(other.gameObject.name);
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Archer>())
        {
            other.GetComponent<Archer>().TakeDamage(damageOnArcher);
            
            //Destroy(other.gameObject);
           // GameObject.FindObjectOfType<Archer>().TakeDamage(damageOnArcher);
            Debug.Log("Sunt EnemyArcher si am dat " + damageOnArcher + "in Archer");
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Soldier>())
        {
            other.GetComponent<Soldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt EnemyArcher si am dat " + damageOnArcher + "in Soldier");
            //Destroy(other.gameObject);
           // GameObject.FindObjectOfType<Soldier>().TakeDamage(damageOnSoldier);
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Barbarian>())
        {
            other.GetComponent<Barbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt EnemyArcher si am dat " + damageOnArcher + "in Barbarian");
            // Destroy(other.gameObject);
           // GameObject.FindObjectOfType<Barbarian>().TakeDamage(damageOnBarbarian);
        }
    }
    IEnumerator DelayAtack()
    {
        yield return new WaitForSeconds(3);
    }
}
