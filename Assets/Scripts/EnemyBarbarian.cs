﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBarbarian : MonoBehaviour {

    public float health = 90f;
    public int range=4;
    public int moneyValue = 80;
    public Transform target;
    public float speed = 6f;
    public float damage = 1f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    private float time = 4f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("Creeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
           // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
             //   Debug.Log("Am gasit un noob");
                return;
            }

        }

    }

    private void Atack()
    {
        GetComponent<Move>().inRange = true;
       // target.GetComponent<EnemyBarbarian>().TakeDamage(damage); 
    }
    public void TakeDamage(float damage)
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            WillTakeDamage = true;
            damageToTake = damage;
        }
    }
    public void Die()
    {
        //GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 4;
        int damageOnSoldier = 2;
        int damageOnBarbarian = 3;

        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Archer>())
        {
           
            //Destroy(other.gameObject);
            GameObject.FindObjectOfType<Archer>().TakeDamage(damageOnArcher);
            Debug.Log("Sunt EnemyBarbarian si am dat " + damageOnArcher + " damage in Archer");
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Soldier>())
        {
            other.GetComponent<Soldier>().TakeDamage(damageOnSoldier);
            //Destroy(other.gameObject);
            GameObject.FindObjectOfType<Soldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt EnemyBarbarian si am dat " + damageOnSoldier + " damage in Soldier");
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Barbarian>())
        {
          
            // Destroy(other.gameObject);
            other.GetComponent<Barbarian>().TakeDamage(damageOnBarbarian);
            GameObject.FindObjectOfType<Barbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt EnemyBarbarian si am dat " + damageOnBarbarian + " damage in Barbarian");
        }
    }
}
