﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyBridge : MonoBehaviour {

    public Button DBridge;
    public GameObject BridgeObject;
    public float time = 5f;

    private bool BridgeDwon = false;
    private Button btn;
    private int DestroyBridge_cost = 300;

	// Use this for initialization
	void Start () {
         btn = DBridge.GetComponent<Button>();
        btn.onClick.AddListener(BridgeDestroy);
        BridgeObject.GetComponent<GameObject>();
       // inactivarea butonului  btn.interactable = false;       
	}
    void Update()
    {
        if (btn.interactable)
        {             
        }else
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                btn.interactable = true;
                BridgeDwon = false;
            }
        }
            //BridgeDestroy();
            //TODO: add delay for bridge....               
    }    
    void OnTriggerEnter(Collider creeps)
    {
        if (BridgeDwon == true)
        {
            if (creeps.CompareTag("Creeps") || creeps.CompareTag("EnemyCreeps"))
            {
                Destroy(creeps.gameObject);
            }
        }
    }
	// Update is called once per frame
    void BridgeDestroy()
    {
        Debug.Log("am distrus podul");
        if (GameObject.FindObjectOfType<MoneyManager>().money >= DestroyBridge_cost)
        {
            GameObject.FindObjectOfType<MoneyManager>().money -= DestroyBridge_cost;
            //Destroy(BridgeObject);
            BridgeDwon = true;
            btn.interactable = false;
            time = 5f;
        }
        else
        {
            Debug.Log("No Money");
        }
    }
}
