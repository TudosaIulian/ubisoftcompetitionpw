﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum CreepType
{ 
    Archer,
    Barbarian,
    Soldier,
    Healer,
    Scorpion
};
public enum StateCreep 
{ 
    Walking, 
    Atacking,
    Idle,
    Dying,
    Dead
};
public class Creep : MonoBehaviour {

	// Use this for initialization
    public CreepType creepType;
    public StateCreep stateCreep;
    public float Health;
    public Image healthBar;
    public int AtackRange;    
    public int Speed;
    public int DamageOnBuildings;
    public float AtackSpeed;
    public int GotMoney;
    public bool IsEnemy;
    public Transform target;
    public int DamagePerLevel = 1;
    public float HealthPerLevel = 5;
    
    public int damageOnArcher = 7;
    public int damageOnSoldier = 2;
    public int damageOnBarbarian = 3;
    public int damageOnHealer = 4;
    public int damageOnScorpion = 2;
    public int damageOnPlayer = 1;

    
    private float maxHealth ;
    private bool WillTakeDamage = false;
    private bool player = false;
    private float damageToTake = 0f;
    private float time = 5f;
    private Transform CanvasHealthBar;
	void Start () {
      
        stateCreep = StateCreep.Walking;
         CanvasHealthBar= transform.FindChild("Canvas");
	}	
	// Update is called once per frame
	void Update () {
        CanvasHealthBar.transform.eulerAngles = new Vector3(40,0,0);
        if (WillTakeDamage)
        {
            Health -= damageToTake;
            HealthBarCreeps();
            if (Health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
            player = false;
        }
        switch (stateCreep)
        {
            case StateCreep.Walking: 
                string CreepTag = IsEnemy ? "Creeps" : "EnemyCreeps";
                if(creepType == CreepType.Healer )
                {
                    CreepTag = "Creeps";
                }
                 GetComponent<Move>().inRange = false;
                 if (IsEnemy == true)
                 { //Script for enemy creeps . They can atack player avatar .                     
                     GameObject Avatar = GameObject.FindGameObjectWithTag("Player");
                     if (Vector3.Distance(Avatar.transform.position, transform.position) <= AtackRange)
                     {
                         target = Avatar.transform;
                         stateCreep = StateCreep.Atacking;
                         break;
                     }
                 } //Script for creeps. They will find the target and atack him.
                 GameObject[] creeps = GameObject.FindGameObjectsWithTag(CreepTag);                
                 foreach (GameObject g in creeps)
                    {
                        if (g == gameObject) continue;
 
                            if (Vector3.Distance(g.transform.position, transform.position) <= AtackRange)
                            {                                                    
                                 target = g.transform;
                                 stateCreep = StateCreep.Atacking;
                                 return;
                             }
                    } //Script for exception (Healer). only healer has this script
                 if (creepType == CreepType.Healer)
                 {
                     CreepTag = "EnemyCreeps";
                     creeps = GameObject.FindGameObjectsWithTag(CreepTag);
                     foreach (GameObject g in creeps)
                     {                 
                             if (Vector3.Distance(g.transform.position, transform.position) <= AtackRange)
                         {
                             target = g.transform;
                             stateCreep = StateCreep.Atacking;
                             break;
                         }
                     }
                 }
                break;
            case StateCreep.Atacking:
                if (target != null && Vector3.Distance(target.position, transform.position) <= AtackRange)
                {
                    BalanceAtack(target.gameObject);
                    time = AtackSpeed;
                    Atack();                    
                }
                else
                {
                    stateCreep = StateCreep.Walking;                    
                }
                break;
            case StateCreep.Idle:                
                        time -= Time.deltaTime;
                        if (time <= 0)
                        {
                            stateCreep = StateCreep.Atacking;
                            time = AtackSpeed;                            
                        }
                break;
            case StateCreep.Dead:
                break;
        }       
	}
    public void Die()
    {
        if (player == true)
        {
            GameObject.FindObjectOfType<MoneyManager>().money += GotMoney;
        }
            stateCreep = StateCreep.Dead;
            Destroy(gameObject);                   
    }
    void BalanceAtack(GameObject other)
    {
        string CreepTag = IsEnemy ? "Creeps" : "EnemyCreeps";
        if (creepType == CreepType.Healer && !other.gameObject.CompareTag(CreepTag))
        {
            Creep creep = other.GetComponent<Creep>();
            creep.Heal();          
        }
        else
        {
            if (other.gameObject.CompareTag(CreepTag) && other.GetComponent<Creep>())
            {
                Creep CreepTest = other.GetComponent<Creep>();
                if (CreepTest.creepType == CreepType.Archer)
                {
                    CreepTest.TakeDamage(damageOnArcher, false);
                }
                else if (CreepTest.creepType == CreepType.Barbarian)
                {
                    CreepTest.TakeDamage(damageOnBarbarian, false);
                }
                else if (CreepTest.creepType == CreepType.Soldier)
                {
                    CreepTest.TakeDamage(damageOnSoldier, false);
                }
                else if (CreepTest.creepType == CreepType.Healer)
                {
                    CreepTest.TakeDamage(damageOnHealer, false);
                }
                else if (CreepTest.creepType == CreepType.Scorpion)
                {
                    CreepTest.TakeDamage(damageOnScorpion, false);
                }
            }
            else if (other.gameObject.CompareTag("Player"))
            {
                PlayerChar Player = other.GetComponent<PlayerChar>();
                Player.TakeDamage(damageOnPlayer, false);
            }
        }
        stateCreep = StateCreep.Idle;        
    }
    private void Atack()
    {
        GetComponent<Move>().inRange = true;       
    }
    public void TakeDamage(float damage,bool playerCommand)
    {
        player = playerCommand; 
        WillTakeDamage = true;
        damageToTake = damage;       
    }
    void Heal()
    {       
        if (Health>1 && Health <=maxHealth)
        {
            Health += 2;
        }        
    }
    void HPInitializate()
    {
        maxHealth = Health;
    }
    void HealthBarCreeps()
    {
      //  Debug.Log(healthBar.fillAmount);
      //  Debug.Log(Health);
      //  Debug.Log(maxHealth);

          healthBar.fillAmount = Health / maxHealth;
    }
    public  void Setlevel(int level)
    {
       // Debug.Log(level);

        Health += (level - 1) * HealthPerLevel;
      //  Debug.Log((level - 1) * HealthPerLevel);
        HPInitializate();
        damageOnArcher += (level - 1) * DamagePerLevel;
        damageOnSoldier += (level - 1) * DamagePerLevel;
        damageOnBarbarian += (level - 1) * DamagePerLevel;
        damageOnHealer += (level - 1) * DamagePerLevel;
        damageOnScorpion += (level - 1) * DamagePerLevel;
        damageOnPlayer += (level - 1) * DamagePerLevel;
       // Debug.Log(Health);
    }
}
