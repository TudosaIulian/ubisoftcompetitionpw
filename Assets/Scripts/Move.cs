﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Move : MonoBehaviour {


    //this is the object what i want to move
    public GameObject Creep;
    public GameObject EnemyCastel;
    public GameObject AllyCastel;
    
    public NavMeshAgent navMeshAgent;
    [System.NonSerialized]
    public bool inRange=false;
    //start
    private Vector3 startPosition;
    //Position to go in finally
    private Vector3 PositionEnemyCastel;
    private Vector3 PositionBaseCastel;
    //end
    private Vector3 endPosition;
    
    //Speed on creeps
    private float SpeedSoldier = 0.5f;
    private float SpeedBarbarian = 0.3f;
    private float SpeedArcher = 0.1f;
    //distance
    private float distance = 30f;
    // Money
   
    public Transform Spawnpoint;

    //time to take from start to end
    private float lerpTime = 15;
    private float currentLerpTime = 0;
   // private bool keyHit = false;
	// Use this for initialization
	void Start () {
        startPosition = Creep.transform.position;
        endPosition = Creep.transform.position + Vector3.forward * distance;
       
        navMeshAgent = GetComponent<NavMeshAgent>();
        
        EnemyCastel = GameObject.FindGameObjectWithTag("EnemyBase");
        AllyCastel = GameObject.FindGameObjectWithTag("Base");
        PositionEnemyCastel = EnemyCastel.transform.position;
        PositionBaseCastel = AllyCastel.transform.position;
        speed();
       
		
	}
	
	// Update is called once per frame
	void Update () {

        destination();
        speed();
      
	}
    void speed()
    {

        if (Creep.name == "Soldier" || Creep.name =="EnemySoldier")
        {
            navMeshAgent.speed =8;
        }
        else if (Creep.name == "Barbarian" || Creep.name == "EnemyBarbarian")
        {           
            navMeshAgent.speed = 6;
        }
        else if (Creep.name == "Archer" || Creep.name == "EnemyArcher")
        {            
            navMeshAgent.speed = 5;
        }
    }
    void moveEnemyLocation()
    {        
        navMeshAgent.SetDestination(PositionBaseCastel);
    }
    void moveAllyLocation()
    {
        navMeshAgent.SetDestination(PositionEnemyCastel);
    }
    void destination()
    {
        if (inRange)
        {
            navMeshAgent.SetDestination(transform.position);
            return;
        }
        if (Creep.tag == "Creeps")
        {
            moveAllyLocation();
        }
        else if (Creep.tag == "EnemyCreeps")
        {
            moveEnemyLocation();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("Creeps") || other.gameObject.CompareTag("EnemyCreeps"))  //|| other.gameObject.CompareTag("Base")
        //{
        //    Destroy(other.gameObject);
        //}
        
    }
  
}
