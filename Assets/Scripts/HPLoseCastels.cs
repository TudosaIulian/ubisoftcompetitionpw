﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HPLoseCastels : MonoBehaviour {

    
    public Slider HPSlider;

    public int HPBase =5;
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       

	}
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log( other.gameObject.name);
        
        if (this.gameObject.CompareTag("Base") && other.GetComponent<Creep>())
        {
            HPBase -= 5;
            Destroy(other.gameObject);
            HPSlider.value -= 5;
          //  Debug.Log(HPBase);
        }else if(this.gameObject.CompareTag("EnemyBase") && other.GetComponent<Creep>())
        {
            HPBase -= 5;
            Destroy(other.gameObject);
            HPSlider.value -= 5;
          //  Debug.Log(HPBase);
           
        }
        if (HPBase < 1)
        {
            Time.timeScale = 0;
            GameOver("TheEnd");

        }
    }
    void GameOver(string name)
    {
        SceneManager.LoadScene(name);

    }
    
}
