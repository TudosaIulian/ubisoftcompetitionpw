﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapHelper : MonoBehaviour {

    public Button DBridge;
    public Button Healer;
    public Button Beast;
    public GameObject BridgeObject;
    public GameObject HealerCreep;
    public GameObject ScorpionCreep;
    public float time = 5f;

    private bool BridgeDwon = false;
    private Button btn;

    // Use this for initialization
    void Start()
    {
        btn = DBridge.GetComponent<Button>();
        btn.onClick.AddListener(BridgeDestroy);
        BridgeObject.GetComponent<GameObject>();
        // inactivarea butonului  btn.interactable = false;

    }
    void Update()
    {
        if (btn.interactable)
        {

        }
        else
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                btn.interactable = true;
                BridgeDwon = false;
            }
        }
        //BridgeDestroy();
        //TODO: add delay for bridge.... 


    }

    void OnTriggerEnter(Collider creeps)
    {
        if (BridgeDwon == true)
        {
            if (creeps.CompareTag("Creeps") || creeps.CompareTag("EnemyCreeps"))
            {
                Destroy(creeps.gameObject);
            }
        }
    }
    // Update is called once per frame
    void BridgeDestroy()
    {
        Debug.Log("am distrus podul");
        //Destroy(BridgeObject);
        BridgeDwon = true;
        btn.interactable = false;
        time = 5f;
    }
}
