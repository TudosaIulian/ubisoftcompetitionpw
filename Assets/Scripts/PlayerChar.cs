﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum PlayerStat
{
    WALKING, IDLE, ATACKING, DYING, DEAD
};
public class PlayerChar : MonoBehaviour {

    public float Health;
    public int AtackRange;
    public int Speed;
    public int DamageOnBuildings;
    public float AtackSpeed;
    public Transform target;
    public int Damage = 55;
    public PlayerStat playerStat;
    public float time = 5f;
    public float CoolDownTime = 3f;
    public int HowMuchYouDie = 0;
    public Animator animator;
    public float vertical;
    public float horizontal;
    public float walk;
    public Slider healtbar;
    // script from youtube melee Combat with unity 
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;
    GameObject character;
    //Stop


    public bool CanAtack = true;

    private bool WillTakeDamage = false;
    private bool player = false;
    private float damageToTake = 0f;
    private bool isDead = false;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        //character
       // character = this.transform.parent.gameObject;
        //stop
        setState(PlayerStat.IDLE);

	}	
	// Update is called once per frame
	void Update () {
        if (WillTakeDamage)
        {
            Health -= damageToTake;
            if (healtbar.value > 1 && healtbar.value < 101)
            {
                healtbar.value -= damageToTake;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                healtbar.value += 50;
                Health += 50;
            }

                if (Health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
            player = false;
        }
        switch (playerStat)
        {
            case PlayerStat.WALKING:
                {
                    CoolDown();
                    //vertical = Input.GetAxis("Vertical");
                    //horizontal = Input.GetAxis("Horizontal");
                  //  Walking();
                    //isDead = false;
                    break;
                }
            case PlayerStat.IDLE:
                {
                    CoolDown();
                    break;
                }
            case PlayerStat.ATACKING:
                {                  
                    target.transform.GetComponent<Creep>().TakeDamage(Damage, true);
                    time = AtackSpeed;
                    CanAtack = false;
                    setState(PlayerStat.IDLE);
                    break;
                }
            case PlayerStat.DYING:
                {
                    //todo ... animatie spre final trece in dead. (event in animatie)
                    break;
                }
            case PlayerStat.DEAD:
                {             
                                      
                    break;
                }       
          }     
	}
    void FixedUpdate()
    {
       // animator.SetFloat("Walk", vertical);
      //  animator.SetFloat("Turn", horizontal);
        //animator.SetFloat("Walking", walk);
    }
    //public void Walking()
    //{
    //    if (Input.GetButton("Fire1"))
    //    {
    //        walk = 0.2f;
    //    }
    //    else
    //    {
    //        walk = 0.0f;
    //    }
    //}
    public void CoolDown()
    {
        time -= Time.deltaTime;
        CoolDownTime -= Time.deltaTime;
        if (time <= 0)
        {
            CanAtack = true;
        }
            
    }
    public void Die()
    {
        if (player == true)
        {
           // GameObject.FindObjectOfType<MoneyManager>().money += GotMoney;
        }
      //  animator.SetBool("Dead", true);
        setState(PlayerStat.DEAD);
        Debug.Log("am murit");
        CanAtack = false;
        time = 5;
        gameObject.SetActive(false);
        
       
       
    }
    public void TakeDamage(float damage, bool playerCommand)
    {
        player = playerCommand;
        WillTakeDamage = true;
        damageToTake = damage;
    }
    public void setState(PlayerStat player)
    {
        playerStat = player;
        animator.SetInteger("State", (int)playerStat);
    }
   
}
