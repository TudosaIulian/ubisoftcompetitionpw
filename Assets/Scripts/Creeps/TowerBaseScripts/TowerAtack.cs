﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAtack : MonoBehaviour {


    public int range = 12;
    public Transform target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("EnemyCreeps");
        GameObject closedCreeps = null;
      // GetComponent<Move>().inRange = false;
       
        foreach (GameObject g in creeps)
        {
            // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
               // Atack();
                HitByTower(closedCreeps);
                //   Debug.Log("Am gasit un noob");
                return;
            }

        }
	}
   
    void HitByTower(GameObject other)
    {
        int damageOnArcher = 3;
        int damageOnSoldier = 1;
        int damageOnBarbarian = 2;

        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<Creep>())
        {
            Creep CreepTest = other.GetComponent<Creep>();
            if (CreepTest.creepType == CreepType.Archer)
            {
                CreepTest.TakeDamage(damageOnArcher,false);
            }
            else if (CreepTest.creepType == CreepType.Barbarian)
            {
                CreepTest.TakeDamage(damageOnBarbarian,false);
            }
            else if (CreepTest.creepType == CreepType.Soldier)
            {
                CreepTest.TakeDamage(damageOnSoldier,false);
            }
        }
    }
}
