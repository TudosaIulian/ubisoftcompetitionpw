﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour {

    public float health = 100f;
    public int range = 2;
    public int moneyValue = 25;
    public Transform target;
    public float damage = 1f;
    public float speed = 8f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("EnemyCreeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
           // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
              //  Debug.Log("Am gasit un noob");
                return;
            }
        }
    }

    private void Atack()
    {
        GetComponent<Move>().inRange = true;        
       // target.GetComponent<Soldier>().TakeDamage(damage);
    }
    public void TakeDamage(float damage)
    {
        WillTakeDamage = true;
        damageToTake = damage;
    }
    public void Die()
    {
        //GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 4;
        int damageOnSoldier = 1;
        int damageOnBarbarian = 3;

        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyArcher>())
        {
            other.GetComponent<EnemyArcher>().TakeDamage(damageOnArcher);
           // GameObject.FindObjectOfType<EnemyArcher>().TakeDamage(damageOnArcher);
            Debug.Log("Sunt Soldier si am dat " + damageOnArcher + " damage in EnemyArcher");
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemySoldier>())
        {
            other.GetComponent<EnemySoldier>().TakeDamage(damageOnSoldier);
           // GameObject.FindObjectOfType<EnemySoldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt Soldier si am dat " + damageOnSoldier + " damage in EnemySoldier");
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyBarbarian>())
        {
            other.GetComponent<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
           // GameObject.FindObjectOfType<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt Soldier si am dat " + damageOnBarbarian + " damage in EnemyBarbarian");
        }
    }
}
