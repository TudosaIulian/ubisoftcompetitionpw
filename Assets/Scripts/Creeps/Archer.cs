﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : MonoBehaviour {


    public float health = 50f;
    public int range = 8;
    public int moneyValue = 55;
    public Transform target;
    public float damage = 1f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    private float time = 5f;
    // Use this for initialization
    int speed = 5;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("EnemyCreeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
            // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
                //   Debug.Log("Am gasit un noob");
                return;
            }

        }


    }

    private void Atack()
    {
        GetComponent<Move>().inRange = true;
        
        //target.GetComponent<Archer>().TakeDamage(damage);
    }
    public void TakeDamage(float damage)
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            DelayAtack();
            WillTakeDamage = true;
            damageToTake = damage;
        }
        
    }
    public void Die()
    {
       // GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 3;
        int damageOnSoldier = 1;
        int damageOnBarbarian = 5;

        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyArcher>())
        {           
           //GameObject.FindObjectOfType<EnemyArcher>().TakeDamage(damageOnArcher);
           other.GetComponent<EnemyArcher>().TakeDamage(damageOnArcher);
           Debug.Log("Sunt Archer si am dat " + damageOnArcher + "in EnemyArcher");
           // DelayAtack();
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemySoldier>())
        {         
            //GameObject.FindObjectOfType<EnemySoldier>().TakeDamage(damageOnSoldier);
            other.GetComponent<EnemySoldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt Archer si am dat " + damageOnSoldier + "in EnemySoldier");
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyBarbarian>())
        {           
            //GameObject.FindObjectOfType<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
            other.GetComponent<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt Archer si am dat " + damageOnBarbarian + "in EnemyBarbarian");
        }
    }
    IEnumerator DelayAtack()
    {
        yield return new WaitForSeconds(3);
    }
}
