﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barbarian : MonoBehaviour {

    public float health = 75f;
    public int range = 4;
    public int moneyValue = 80;
    public Transform target;
    public float speed = 6f;
    public float damage = 1f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    private float time = 5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps = GameObject.FindGameObjectsWithTag("EnemyCreeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
            // Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {

                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
                //   Debug.Log("Am gasit un noob");
                return;
            }

        }

    }

    private void Atack()
    {
     
            GetComponent<Move>().inRange = true;
        
        // target.GetComponent<EnemyBarbarian>().TakeDamage(damage); 
    }
    public void TakeDamage(float damage)
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            WillTakeDamage = true;
            damageToTake = damage;
        }
    }
    public void Die()
    {
       // GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 7;
        int damageOnSoldier = 2;
        int damageOnBarbarian = 3;

        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyArcher>())
        {           
            //GameObject.FindObjectOfType<EnemyArcher>().TakeDamage(damageOnArcher);
            other.GetComponent<EnemyArcher>().TakeDamage(damageOnArcher);
            Debug.Log("Sunt Barbarian si am dat " + damageOnArcher + " damage in EnemyArcher");
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemySoldier>())
        {
            //GameObject.FindObjectOfType<EnemySoldier>().TakeDamage(damageOnSoldier);
            other.GetComponent<EnemySoldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt Barbarian si am dat " + damageOnSoldier + " damage in EnemySoldier");
        }
        if (other.gameObject.CompareTag("EnemyCreeps") && other.GetComponent<EnemyBarbarian>())
        {
            // GameObject.FindObjectOfType<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
            other.GetComponent<EnemyBarbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt Barbarian si am dat " + damageOnBarbarian + " damage in EnemyBarbarian");
        }
    }
}
