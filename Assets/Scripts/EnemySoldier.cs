﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoldier : MonoBehaviour {

    public float health = 90f;
    public int range = 2;
    public int moneyValue = 25;
    public Transform target;
    public float damage = 1f;
    public float speed = 8f;

    private bool WillTakeDamage = false;
    private float damageToTake = 0f;
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (WillTakeDamage)
        {
            health -= damageToTake;
            if (health <= 0)
            {
                Die();
                return;
            }
            WillTakeDamage = false;
        }
        GameObject[] creeps =  GameObject.FindGameObjectsWithTag("Creeps");
        GameObject closedCreeps = null;
        GetComponent<Move>().inRange = false;
        foreach (GameObject g in creeps)
        {
            //Debug.Log(Vector3.Distance(g.transform.position, transform.position));
            //Debug.Log("Am intrat in foreach");
            if (Vector3.Distance(g.transform.position, transform.position) <= range)
            {                
                closedCreeps = g;
                target = g.transform;
                BalanceAtack(closedCreeps);
                Atack();
              //  Debug.Log("Am gasit un noob");
                return;
            }
        }                      
	}

    private void Atack()
    {
        GetComponent<Move>().inRange = true;
        //target.GetComponent<EnemySoldier>().TakeDamage(damage);
    }
    public void TakeDamage(float damage)
    {
        WillTakeDamage = true;
        damageToTake = damage;
    }
    public void Die()
    {
       // GameObject.FindObjectOfType<MoneyManager>().money += moneyValue;
        Destroy(gameObject);
    }
    void BalanceAtack(GameObject other)
    {
        int damageOnArcher = 4;
        int damageOnSoldier = 1;
        int damageOnBarbarian = 3;

        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Archer>())
        {            
            //Destroy(other.gameObject);
            GameObject.FindObjectOfType<Archer>().TakeDamage(damageOnArcher);
            Debug.Log("Sunt EnemySoldier si am dat " + damageOnArcher + "in EnemyArcher");
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Soldier>())
        {
            other.GetComponent<Soldier>().TakeDamage(damageOnSoldier);
            //Destroy(other.gameObject);
            GameObject.FindObjectOfType<Soldier>().TakeDamage(damageOnSoldier);
            Debug.Log("Sunt EnemySoldier si am dat " + damageOnSoldier + "in EnemySoldier");
        }
        if (other.gameObject.CompareTag("Creeps") && other.GetComponent<Barbarian>())
        {           
            // Destroy(other.gameObject);
            other.GetComponent<Barbarian>().TakeDamage(damageOnBarbarian);
            GameObject.FindObjectOfType<Barbarian>().TakeDamage(damageOnBarbarian);
            Debug.Log("Sunt EnemySoldier si am dat " + damageOnBarbarian + "in EnemyBarbarian");
        }
    }
}
