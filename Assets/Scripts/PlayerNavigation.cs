﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class PlayerNavigation : MonoBehaviour {


    public Camera cam;
    public NavMeshAgent navMeshAgent;
    public Text countTextMoney;
    public Animator animator;
    public bool canMove;
    public bool pathReached;

    private int countMoney;  
   
    private float minDistanceReachedPoint = 2f;
    private  PlayerChar playerChar;
    private Vector3 destination;
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();
        playerChar =  GetComponent<PlayerChar>();      
        destination = this.transform.position;
        canMove = true;
        pathReached = false;
	}	
	// Update is called once per frame
	void Update () {
        Debug.DrawLine(destination, destination +new Vector3(0, 100, 0),Color.red);
        //Move the player with left click on the mouse
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
           
            if (Physics.Raycast(ray, out hit))
            {//when raycast hit something
                if (hit.transform.GetComponent<Creep>() != null)
                {//when we have a creep
                    if (hit.transform.GetComponent<Creep>().IsEnemy)
                    {//when have an enemy
                        if (Vector3.Distance(hit.transform.position, transform.position) <= playerChar.AtackRange && playerChar.CanAtack == true)
                        {                           
                            pathReached = false;
                            playerChar.target = hit.transform;                           
                            playerChar.setState(PlayerStat.ATACKING);
                        }
                        else
                        {                           
                            if (Vector3.Distance(hit.transform.position, transform.position) > playerChar.AtackRange)
                            {
                                navMeshAgent.SetDestination(hit.point);
                                pathReached = false;                               
                                playerChar.setState(PlayerStat.WALKING);
                                destination = hit.point;
                            }                           
                        }
                    }
                    else
                    {
                        //pathReached = false;
                        playerChar.setState(PlayerStat.WALKING);
                        //navMeshAgent.SetDestination(hit.point);
                        //destination = hit.point;                     
                    }
                }
                else
                {
                    pathReached = false;
                    playerChar.setState(PlayerStat.WALKING);                   
                    navMeshAgent.SetDestination(hit.point);
                    destination = hit.point;
                }
            }
            else
            {        playerChar.setState(PlayerStat.IDLE);              
            }
        }
        else
        {
            if (Vector3.Distance(this.transform.position, destination) < minDistanceReachedPoint && pathReached == false)
           {   
                playerChar.setState(PlayerStat.IDLE);              
                pathReached = true;               
           }            
        }		
	}       
}
