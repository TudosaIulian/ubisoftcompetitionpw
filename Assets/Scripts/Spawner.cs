﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

	// Use this for initialization
    public Button button_Spawn;
    public GameObject spawn;
    public GameObject Bridge;
    public int Level = 1;

    private int Archer_cost = 40;
    private int Soldier_cost = 25;
    private int Barbarian_cost = 60;   
    private int Healer_cost = 150;
    private int Beast_cost = 1000;

    private int Archer_upgrade_cost = 120;
    private int Barbarian_upgrade_cost = 150;
    private int Soldier_upgrade_cost = 135;
    private int Healer_upgrade_cost = 150;

    private int LevelArcher = 1;
    private int LevelBarbarian = 1;
    private int LevelSoldier = 1;
    private int LevelHealer = 1;
    
    //static public Spawner i;
    void Start()
    {
        Button btn = button_Spawn.GetComponent<Button>();
       // btn.onClick.AddListener(TaskOnClick);
        if (gameObject.CompareTag("Spawner_Archer"))
        {
            btn.onClick.AddListener(Spawn_Archer);
        }
        else if (gameObject.CompareTag("Spawner_Soldier"))
        {
            btn.onClick.AddListener(Spawn_Soldier);
        }
        else if (gameObject.CompareTag("Spawner_Barbarian"))
        {
            btn.onClick.AddListener(Spawn_Barbarian);
        }
        else if (gameObject.CompareTag("Bridge"))
        {
            btn.onClick.AddListener(BridgeFunction);
        }
        else if (gameObject.CompareTag("Spawner_Healer"))
        {
            btn.onClick.AddListener(Spawn_Healer);
        }
        else if (gameObject.CompareTag("Spawner_Beast"))
        {
            btn.onClick.AddListener(Spawn_Beast);
        }
        else if (gameObject.CompareTag("UpgradeSoldier"))
        {
            btn.onClick.AddListener(UpgradeSoldier);
        }
        else if (gameObject.CompareTag("UpgradeArcher"))
        {
            btn.onClick.AddListener(UpgradeArcher);
        }
        else if (gameObject.CompareTag("UpgradeBarbarian"))
        {
            btn.onClick.AddListener(UpgradeBarbarian);
        }
        else if (gameObject.CompareTag("UpgradeHealer"))
        {
            btn.onClick.AddListener(UpgradeHealer);
        }
        
        //Debug.Log("click1!" + spawn);
        //Instantiate(spawn, transform.position, Quaternion.identity);
        //Debug.Log("click 2" + spawn);
        //Instantiate(spawn, transform.position, Quaternion.identity);        
    }
    public void BridgeFunction()
    {
       
        Bridge.SetActive(false);
       
    }

    // void TaskOnClick()
    //{
    //  //  Debug.Log("Y " + Spawner.i.spawn);
    //   // Instantiate(Spawner.i.spawn, transform.position, Quaternion.identity);       
       
    //}
    public void Spawn_Healer()
    {

        if (GameObject.FindObjectOfType<MoneyManager>().money >= Healer_cost)
        {
            GameObject.FindObjectOfType<MoneyManager>().money -= Healer_cost;
            GameObject healer = GameObject.FindGameObjectWithTag("Spawner_Healer").GetComponent<Spawner>().spawn;
            int LHealer = GameObject.FindGameObjectWithTag("Spawner_Healer").GetComponent<Spawner>().Level;
            GameObject GHealer = (GameObject)  Instantiate(healer, transform.position, Quaternion.identity);
            GHealer.GetComponent<Creep>().Setlevel(LHealer);
        }
        else
        {
            Debug.Log("No Money!");
        }

    }
    public void Spawn_Beast()
    {

        if (GameObject.FindObjectOfType<MoneyManager>().money >= Beast_cost)
        {
            GameObject.FindObjectOfType<MoneyManager>().money -= Beast_cost;
            GameObject scorpion = GameObject.FindGameObjectWithTag("Spawner_Beast").GetComponent<Spawner>().spawn;
            Instantiate(scorpion, transform.position, Quaternion.identity);
        }
        else
        {
            Debug.Log("No Money!");
        }

    }
     public void Spawn_Archer()
     {         
       
        if (GameObject.FindObjectOfType<MoneyManager>().money >= Archer_cost)
        {
            GameObject.FindObjectOfType<MoneyManager>().money -= Archer_cost;
            GameObject archer = GameObject.FindGameObjectWithTag("Spawner_Archer").GetComponent<Spawner>().spawn;
            int LArcher = GameObject.FindGameObjectWithTag("Spawner_Archer").GetComponent<Spawner>().Level;
           GameObject GArcher= (GameObject) Instantiate(archer, transform.position, Quaternion.identity);
           GArcher.GetComponent<Creep>().Setlevel(LArcher);
        }
        else
        {
            Debug.Log("No Money!");
        }
         
     }
     void Spawn_Soldier()
     {
        
         if (GameObject.FindObjectOfType<MoneyManager>().money >= Soldier_cost)
        {
         GameObject.FindObjectOfType<MoneyManager>().money -= Soldier_cost;
         GameObject soldier = GameObject.FindGameObjectWithTag("Spawner_Soldier").GetComponent<Spawner>().spawn;
         int LSoldier = GameObject.FindGameObjectWithTag("Spawner_Soldier").GetComponent<Spawner>().Level;
             GameObject GSoldier = (GameObject)  Instantiate(soldier, transform.position, Quaternion.identity);
             GSoldier.GetComponent<Creep>().Setlevel(LSoldier);
        }
          else
          {
              Debug.Log("No Money!");
          }
     }
     void Spawn_Barbarian()
     {
         
         if (GameObject.FindObjectOfType<MoneyManager>().money >= Barbarian_cost)
        {
         GameObject.FindObjectOfType<MoneyManager>().money -= Barbarian_cost;
         GameObject barbarian = GameObject.FindGameObjectWithTag("Spawner_Barbarian").GetComponent<Spawner>().spawn;
         int LBarbarian = GameObject.FindGameObjectWithTag("Spawner_Barbarian").GetComponent<Spawner>().Level;
             GameObject GBarbarian = (GameObject)   Instantiate(barbarian, transform.position, Quaternion.identity);
             GBarbarian.GetComponent<Creep>().Setlevel(LBarbarian);
        }
         else
         {
             Debug.Log("No Money!");
         }
     }
     public void UpgradeArcher()
     {
         if (GameObject.FindObjectOfType<MoneyManager>().money >= Archer_upgrade_cost)
         {
             GameObject.FindObjectOfType<MoneyManager>().money -= Archer_upgrade_cost;
             GameObject.FindGameObjectWithTag("Spawner_Archer").GetComponent<Spawner>().Level++;
             Debug.Log(LevelArcher);
         }
         else
         {
             Debug.Log("No Money!");
         }
     }
     public void UpgradeSoldier()
     {
          if (GameObject.FindObjectOfType<MoneyManager>().money >= Soldier_upgrade_cost)
         {
             GameObject.FindObjectOfType<MoneyManager>().money -= Soldier_upgrade_cost;
             GameObject.FindGameObjectWithTag("Spawner_Soldier").GetComponent<Spawner>().Level++;
         }
         else
         {
             Debug.Log("No Money!");
         }              
     }
     public void UpgradeBarbarian()
     {        
         if (GameObject.FindObjectOfType<MoneyManager>().money >= Barbarian_upgrade_cost)
         {
             GameObject.FindObjectOfType<MoneyManager>().money -= Barbarian_upgrade_cost;
             Debug.Log(LevelBarbarian);
             GameObject.FindGameObjectWithTag("Spawner_Barbarian").GetComponent<Spawner>().Level++;
             Debug.Log(LevelBarbarian);
         }
         else
         {
             Debug.Log("No Money!");
         }              
     }
     public void UpgradeHealer()
     {
         if (GameObject.FindObjectOfType<MoneyManager>().money >= Healer_upgrade_cost)
         {
             GameObject.FindObjectOfType<MoneyManager>().money -= Healer_upgrade_cost;
             GameObject.FindGameObjectWithTag("Spawner_Healer").GetComponent<Spawner>().Level++;
         }
         else
         {
             Debug.Log("No Money!");
         }             
     }
}
