﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {

    public enum SpawnState
    {
        SPAWNING, WAITING, COUNTING
    };

    [System.Serializable]
    public class WaveSpawn
    {
        public string name;       
        public int count;
        public float rate;

    }
    public WaveSpawn[] waves;
    public Transform[] EnemyCreeps;
    public Transform[] spawnPoints;
    
    private int nextWave = 0;
    
    public float timeBetweenWaves = 5f;
    public float waveCountDown;

    private float searchCountdown = 1f;

    private SpawnState state = SpawnState.COUNTING;
    void Start()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced.");
        }
        waveCountDown = timeBetweenWaves;
    }
    void Update()
    {
        if (spawnPoints.Length == 0)
        {
            return;
        }
        if (state == SpawnState.WAITING)
        { 
           //check if enemies are still alive
            if (!EnemyIsAlive())
            {
                //Begin a new round
                WaveCompleted();
            }
            else
            {
                return;
            }
        }
        if (waveCountDown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                //Start spawning wave
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }
    void WaveCompleted()
    {
        Debug.Log("Wave completed!");

        state = SpawnState.COUNTING;
        waveCountDown = timeBetweenWaves;
        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("Completed all waves!");
            return;
        }
        else
        {
            nextWave++;
        }
    }
    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;

            if (GameObject.FindGameObjectWithTag("EnemyCreeps") == null)
            {
                return false;
            }
        }
        return true;
    }
    IEnumerator SpawnWave(WaveSpawn _Wave)
    {
        Debug.Log("Spawning Wave: " + _Wave.name);
        state = SpawnState.SPAWNING;

        //Spawn
        for (int i = 0; i < _Wave.count; i++)
        {
           // SpawnEnemy(_Wave.enemy);
            
            int contorSpawnCreep = Random.Range(0, 3);
            SpawnEnemy(EnemyCreeps[contorSpawnCreep]);

           // Debug.Log(contorSpawnCreep);
            yield return new WaitForSeconds(1f / _Wave.rate);
        }


            state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    { 
        //Spawn enemy
       // Debug.Log("Spawning enemt: "+_enemy.name);
      
        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
      //  Instantiate(_enemy, transform.position, transform.rotation);
        Transform t = (Transform) Instantiate(_enemy, transform.position, transform.rotation);
        t.GetComponent<Creep>().Setlevel(nextWave+1);

    }
}

